import java.util.Scanner;

public class AddTwoMatrices {

	public static void main(String args[]) {
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Nhap kich thuoc cua ma tran vuong: ");
	    int n = keyboard.nextInt();
	    int a[][] = new int[n][n];
	    int b[][] = new int[n][n];
	    for (int i = 0; i < n; i++) {
	        for (int j = 0; j < n; j++) {
	            System.out.print("a [" + (i+1) + ", " + (j+1) + "]: ");
	            a[i][j] = keyboard.nextInt();
	        }
	    }
	    for (int i = 0; i < n; i++) {
	        for (int j = 0; j < n; j++) {
	            System.out.print("b [" + (i+1) + ", " + (j+1) + "]: ");
	            b[i][j] = keyboard.nextInt();
	        }
	    }
	    int c[][] = new int[n][n];
	    System.out.println("Tong cua ma tran a va b la: ");
	    for (int i = 0; i < n; i++) {
	        for (int j = 0; j < n; j++) {
	            c[i][j] = a[i][j] + b[i][j];
	            System.out.print(c[i][j]+" ");
	        }
	        System.out.println();
	    }
	    keyboard.close();
	}

}
