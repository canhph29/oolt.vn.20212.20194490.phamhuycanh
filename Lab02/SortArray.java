import java.util.*;

public class SortArray {

	public static void main(String[] args) {
        int n, sum = 0;
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Nhap so phan tu cua mang A: ");
        n = keyboard.nextInt();
        int a[] = new int[n];
        for(int i = 0; i < n ; i++) {
        	System.out.print("A[" + i+ "] = ");
        	keyboard.nextLine();
            a[i] = keyboard.nextInt();                  
            sum = sum + a[i];
        }
        System.out.print("Mang truoc khi sap xep: ");
        System.out.printf("%s", Arrays.toString(a));
        System.out.println();
        
        System.out.print("Mang sau khi sap xep: ");
        Arrays.sort(a);
        System.out.printf("%s", Arrays.toString(a));
        System.out.println();
        
        System.out.println("Tong cac phan tu trong mang: "+sum);
        
        System.out.println("Gia tri trung binh cac phan tu trong mang: " + (float)sum/n);
        
        keyboard.close();
    }

}
