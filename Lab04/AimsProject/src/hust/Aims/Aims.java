package hust.Aims;

import hust.DigitalVideoDisc.DigitalVideoDisc;
import hust.Order.Order;

public class Aims {
	public static void main (String [] args) {
		Order anOrder = new Order();
		
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King", "Animation", "Nature", 90, 10);
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Tom and Jerry", "Animation", "Funny", 20, 9);
		anOrder.addDigitalVideoDisc(dvd1, dvd2);
		anOrder.showOrder();
		
		//TODO: add item theo list dvd3,4,5
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("King Kong", "Action", "ABC", 80, 20.0f);
		DigitalVideoDisc dvd4 = new DigitalVideoDisc("Zootopia","Animation", "CDE", 90, 30.00f);
		DigitalVideoDisc dvd5 = new DigitalVideoDisc("Tom and Jerry", "Animation","We",120, 100.0f);
		DigitalVideoDisc [] dvdlist = {dvd3, dvd4, dvd5};	
		anOrder.addDigitalVideoDisc(dvdlist);
		anOrder.showOrder();
	}
}
