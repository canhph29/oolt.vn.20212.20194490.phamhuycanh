package hust.TestPassingParameter;

import hust.DigitalVideoDisc.DigitalVideoDisc;

public class TestPassingParameter {
	public static void main(String[] args) {
		DigitalVideoDisc jungleDVD = new DigitalVideoDisc("JungLe");
		DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderella");

		swap(jungleDVD, cinderellaDVD);
		System.out.println("jungle title : " + jungleDVD.getTitle());
		System.out.println("cinderella title : " + cinderellaDVD.getTitle());

		changeTitle(jungleDVD, cinderellaDVD.getTitle());
		System.out.println("jungle title : " + jungleDVD.getTitle());
		System.out.println("cinderella title : " + cinderellaDVD.getTitle());
	}

	public static void swap1(Object o1, Object o2) {
		Object tmp;
		tmp = o1;
		o1 = o2;
		o2 = tmp;
	}

	public static void swap(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		DigitalVideoDisc temp;
		temp = dvd1;
		dvd1 = dvd2;
		dvd2 = temp;
	}
	
	public static void changeTitle(DigitalVideoDisc dvd, String title) {
//		String oldTitle = dvd.getTitle();
		dvd.setTitle(title);

	}

}


