package hust.DigitalVideoDisc;

public class DigitalVideoDisc {
	private String title;
	private String category;
	private String director;
	private int length;
	private float cost;

	//TODO:Constructor
	public DigitalVideoDisc(String title) {
		super();
		this.title = title;
	}
	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
		super();
		this.title = title;
		this.category = category;
		this.director = director;
		this.length = length;
		this.cost = cost;
	}


	//TODO:Getter & Setter
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	//TODO:showinfor
	public void showInfor(DigitalVideoDisc dvd) {
		System.out.println("DVD Infor ");
		System.out.println("Title	 : " + this.title);
		System.out.println("Category : " + this.category);
		System.out.println("Director : " + this.director);
		System.out.println("Length   : " + this.length);
		System.out.println("Cost   	 : " + this.cost);
	}
}
