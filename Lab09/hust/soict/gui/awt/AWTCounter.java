/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hust.soict.gui.awt;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 *
 * @author ADMIN
 */
public class AWTCounter extends Frame{
    // Khai bao cac component tren giao dien
    private Label lblCount;
    private TextField tfCount;
    private Button btnCount;
    private int count; // bien chua gia tri dem
    
    // Constrcutor cho AWTCounter: khoi tao cua so giao dien
    public AWTCounter(){
        // Thiet lap layout cho cua so giao dien
        // FlowLayout: bo cuc dang don gian: component nao duoc them vao truoc se xuat hien truoc tren 1 hang
        // neu ko du cho thi xuong hang moi
        this.setLayout(new FlowLayout());
        
        // Khoi tao cac component va them vao giao dien
        lblCount = new Label("Counter");
        this.add(lblCount);
        
        tfCount = new TextField(count + "", 10);
        tfCount.setEditable(true);
        this.add(tfCount);
        
        btnCount = new Button("Midori");
        this.add(btnCount);
        // Dang ky lang nghe su kien tren button
        btnCount.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ++count;
                tfCount.setText(count + "");
                       
            }
        });
               
        
        //Thiet lap thong tin cho cua so giao dien
        this.setTitle("Kawasemi AWT Counter");
        this.setSize(400, 150);
        this.setVisible(true);
        
        this.addWindowListener(new WindowAdapter() {
        @Override
        public void windowClosing(WindowEvent we) {
            dispose();
        }
     }
);
    }
    public static void main(String[] args) {
        AWTCounter app = new AWTCounter();
        
    }

}
