package hedspi.oolt.labs;

public class Order {
	//Khai bao mot hang so: So luong san pham toi da
	public static final int MAX_NUMBERS_ORDERED = 10;
	//Khai bao mot mang (Array) cac doi tuong DVD
	private DigitalVideoDisc itemsOrder[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	// Khai bao thuoc tinh chua so luong phan tu hien co trong don hang
	private int qtyOrdered = 0;
	
	//Xay dung cac phuong thuc get / set
	public int getGtyOrdered() {
		return qtyOrdered;
	}
	public void setGtyOrdered(int gtyOrdered) {
		this.qtyOrdered = gtyOrdered;
	}
	
	//Xay dung phuong thuc them mot doi tuong DVD vao don hang
	//Noi dung: them mot doi tuong vao mang itemsOrdered[] 
	// -> Phai kiem tra xem mang da bi day (full) chua
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (this.qtyOrdered == MAX_NUMBERS_ORDERED)
			System.out.println("The order is almost full");
		else {
			this.itemsOrder[qtyOrdered] = disc;
			qtyOrdered++;
			System.out.println("The disc has been added");
			System.out.println("Total disc: " + this.qtyOrdered);
		}
	}
	
	//Phuong thuc xoa mot doi tuong DVD khoi don hang
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		//Viet lenh loai bo doi tuong DVD khoi mang
		//Luu y kiem tra tinh huong mang rong (khong co phan tu)
		 String cmp = disc.getTitle();
		 int index = 0;
	        if (qtyOrdered == 0) {
	            System.out.println("Empty array");
	        } else {
	            for (int i = 0; i < qtyOrdered; i++) {
	                if (itemsOrder[i].getTitle().equals(cmp)) {
	                	System.out.println("The disc has been removed");
	                    for(int j = i; j < qtyOrdered - 1; j++)
	                    {
	                        itemsOrder[j] = itemsOrder[j+1];
	                    }
	                    itemsOrder[qtyOrdered - 1] = null;
	                    qtyOrdered--;
	                    index++;
	                }
	            }
	            if(index == 0) {
                	System.out.println("The disc not found");
                }
	        }
	}
	
	//Phuong thuc tinh tong gia tri cua don hang
	public float totalCost() {
		float total = 0.0f;
		for (int i = 0; i < this.qtyOrdered; i++)
			total += itemsOrder[i].getCost();
		return total;
	}
}
















