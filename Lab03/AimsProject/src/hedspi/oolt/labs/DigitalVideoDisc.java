package hedspi.oolt.labs;

public class DigitalVideoDisc {
	// Khai bao cac thuoc tinh
	private String title;
	private String category;
	private String director;
	private int length;
	private float cost;
	
	//Xay dung cac phuong thuc
	//1. Cac phuong thuc get/set
	public String getTitle() {
		return this.title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		if (length >= 0) 
			this.length = length;
		else 
			this.length = 0;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		if (cost > 0)
			this.cost = cost;
		else
			this.cost = 0.0f;
	}
	
	//2. Cac phuong thuc khoi tao (Constructor)
	//  - Nhiem vu cua Constructor: Tao vung nho (memory) chua thong tin cua Object va thiet lap / gan gia tri cho cac thuoc tinh cua Object
	//  - Dac diem cua Constuctor:
	//		+ Ten cua Constructor trung voi ten lop
	//		+ Khong co kieu tra ve, khong dung tu khoa void
	//	- Co the xay dung nhieu Constructor voi tham so khac nhau -> Giup khoi tao doi tuong theo nhieu cach
	
	// 	2.1 Constructor khong tham so
	public DigitalVideoDisc() {
		this.title = "";
		this.category = "";
		this.director = "";
		this.length = 0;
		this.cost = 0.0f;
	}
	
	//	2.2 Constructor co 1 tham so: Create DVD object by title
	public DigitalVideoDisc(String title) {
		this.title = title;
	}
	
	//	2.3 Constructor co 2 tham so
	public DigitalVideoDisc(String title, String category) {
		super();
		this.title = title;
		this.category = category;
	}
	
	//	2.4 Constructor co 3 tham so
	public DigitalVideoDisc(String title, String category, String director) {
		super();
		this.title = title;
		this.category = category;
		this.director = director;
	}
	
	//	2.5 Constructor with all attributes
	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
		super();
		this.title = title;
		this.category = category;
		this.director = director;
		this.length = length;
		this.cost = cost;
	}
	
	//3. Cac phuong thuc khac
	// 	Phuong thuc in thong tin cua doi tuong DVD
	public void printInfo() {
		System.out.println("-----DVD Info-----");
		System.out.println("Title: " + this.title);
		System.out.println("Category: " + this.category);
		System.out.println("Director: " + this.director);
		System.out.println("Length: " + this.length);
		System.out.println("Cost: " + this.cost);
	}
	
}
























