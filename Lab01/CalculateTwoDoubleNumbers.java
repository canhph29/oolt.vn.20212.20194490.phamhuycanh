import javax.swing.JOptionPane;
public class CalculateTwoDoubleNumbers {
	public static void main(String[] args) {
		String strNum1, strNum2;
		
		strNum1 = JOptionPane.showInputDialog(null, "Please input the first number: ", "Input the first number: ", JOptionPane.INFORMATION_MESSAGE);
		double num1 = Double.parseDouble(strNum1);
		strNum2 = JOptionPane.showInputDialog(null, "Please input the second number: ", "Input the second number: ", JOptionPane.INFORMATION_MESSAGE);
		double num2 = Double.parseDouble(strNum2);
		double add = num1 + num2;
		double sub = num1 - num2;
		double mul = num1 * num2;
		double div = num1 / num2;
		double rem = num1 % num2;
		JOptionPane.showMessageDialog(null, "Tong: " + add);
		JOptionPane.showMessageDialog(null, "Hieu: " + sub);
		JOptionPane.showMessageDialog(null, "Tich: " + mul);
		JOptionPane.showMessageDialog(null, "Thuong: " + (int) div +" Du: " + rem);
		System.exit(0);
	}
}


