// Giai phuong trinh bac hai ax^2 + bx + c = 0
import javax.swing.JOptionPane;

public class PhuongTrinhBacHai {
	public static void main(String args[]){
		
        double a = Double.parseDouble(JOptionPane.showInputDialog("Nhap a"));
        double b = Double.parseDouble(JOptionPane.showInputDialog("Nhap b"));
        double c = Double.parseDouble(JOptionPane.showInputDialog("Nhap c"));
        double delta = b*b - 4*a*c;
        
        if (delta == 0)
            JOptionPane.showMessageDialog(null, "Phuong trinh co nghiem kep:\n x1 = x2 = " + -b/(2*a));
        else if (delta > 0)
            JOptionPane.showMessageDialog(null, "Phuong trinh co 2 nghiem phan biet:\nx1 = " + (-b+Math.sqrt(delta))/(2*a) 
            																	 + "\nx2 = " + (-b-Math.sqrt(delta))/(2*a));
        else
            JOptionPane.showMessageDialog(null, "Phuong trinh vo nghiem");
        
        System.exit(0);
    }
}
