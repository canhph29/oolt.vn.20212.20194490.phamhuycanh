// Giai he phuong trinh bac nhat hai an

import javax.swing.JOptionPane;

public class HePhuongTrinhBacNhat {
	public static void main(String args[]){
		
        double a11 = Double.parseDouble(JOptionPane.showInputDialog("Nhap a11"));
        double a12 = Double.parseDouble(JOptionPane.showInputDialog("Nhap a12"));
        double b1 = Double.parseDouble(JOptionPane.showInputDialog("Nhap b1"));
        
        double a21 = Double.parseDouble(JOptionPane.showInputDialog("Nhap a21"));
        double a22 = Double.parseDouble(JOptionPane.showInputDialog("Nhap a22"));
        double b2 = Double.parseDouble(JOptionPane.showInputDialog("Nhap b2"));
        
        double D, D1, D2;
        D = a11 * a22 - a21 * a12;
        D1 = b1 * a22 - b2 * a12;
        D2 = a11 * b2 - a21 * b1;
        
        if (D == 0) {
            if (D1 == 0 && D2 == 0)
                JOptionPane.showMessageDialog(null, "He phuong trinh co vo so nghiem");
            else
                JOptionPane.showMessageDialog(null, "He phuong trinh vo nghiem");
        } else {
            JOptionPane.showMessageDialog(null, "He phuong trinh co nghiem duy nhat:\n (x1,x2) = (" + D1/D + "," + D2/D + ")");
        }
        
        System.exit(0);
    }
}
