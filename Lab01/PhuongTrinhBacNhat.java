// Giai phuong trinh bac nhat ax + b = 0
import javax.swing.JOptionPane;

public class PhuongTrinhBacNhat {
	public static void main(String args[]){
        Double a = Double.parseDouble(JOptionPane.showInputDialog("Nhap a"));
        Double b = Double.parseDouble(JOptionPane.showInputDialog("Nhap b"));
        if (a == 0) {
            if (b == 0)
                JOptionPane.showMessageDialog(null, "Phuong trinh co vo so nghiem", "Phuong trinh " + a + "x + " + b +" = 0", JOptionPane.INFORMATION_MESSAGE);
            else
                JOptionPane.showMessageDialog(null, "Phuong trinh vo nghiem", "Phuong trinh " + a + "x + " + b + " = 0", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "Phuong trinh co nghiem duy nhat\nx = " + -b/a, "Phuong trinh " + a + "x + " + b + " = 0", JOptionPane.INFORMATION_MESSAGE);
        }
        System.exit(0);
    }
}
