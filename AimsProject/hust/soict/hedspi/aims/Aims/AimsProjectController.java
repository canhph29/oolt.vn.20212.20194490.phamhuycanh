package hust.soict.hedspi.aims.Aims;

import hust.soict.hedspi.aims.order.Order;
import javafx.animation.PauseTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AimsProjectController implements Initializable {
    public AnchorPane ap;
    public Button createButton;
    public Button addButton;
    public Button delButton;
    public Button displayButton;
    public Button exitButton;
    public Label text;

    static Order order = null;
    Popup pp = null;

    private void initButton(Button button) {
        button.setOnMouseEntered(e -> button.setStyle("-fx-background-color: #fd8a4f; -fx-border-radius: 20px; " +
                "-fx-background-radius: 20px; -fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.8), 10, 0, 0, 0);"));
        button.setOnMouseExited(e -> button.setStyle("-fx-background-color: #fd8a4f; -fx-border-radius: 20px; " +
                "-fx-background-radius: 20px;"));
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        initButton(createButton);
        initButton(addButton);
        initButton(delButton);
        initButton(displayButton);
        initButton(exitButton);

    }

    private void alert(String message) {
        Stage alert = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("alert.fxml"));
        Parent pr;
        try {
            pr = loader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Alert controller = loader.getController();
        controller.setTextAlert(message);
        Scene sc1 = new Scene(pr);
        alert.setScene(sc1);
        sc1.setFill(Color.TRANSPARENT);
        alert.initStyle(StageStyle.TRANSPARENT);
        alert.setX(ap.getScene().getWindow().getX() + 75);
        alert.setY(ap.getScene().getWindow().getY() + 100);
        alert.setAlwaysOnTop(true);
        alert.show();
        PauseTransition delay = new PauseTransition(Duration.seconds(2));
        delay.setOnFinished(event -> alert.close());
        delay.play();
    }

    private void popup() {
        Stage popup = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("popup.fxml"));
        Parent pr;
        try {
            pr = loader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        pp = loader.getController();
        Scene sc1 = new Scene(pr);
        popup.setScene(sc1);
        sc1.setFill(Color.TRANSPARENT);
        popup.initStyle(StageStyle.TRANSPARENT);
        popup.setX(ap.getScene().getWindow().getX() + 45);
        popup.setY(ap.getScene().getWindow().getY() + 150);
        popup.setAlwaysOnTop(true);
        popup.show();
    }

    public void create(ActionEvent actionEvent) {
        order = Order.createOrder();
        alert("Tạo đơn hàng mới thành công");
    }

    public void add(ActionEvent actionEvent) {
        if(order == null){
            alert("Bạn cần tạo đơn hàng trước");
        }else {
            popup();
            pp.add(order);
        }
    }

    public void delete(ActionEvent actionEvent) {
        if(order == null){
            alert("Bạn cần tạo đơn hàng trước");
        }else {
            popup();
            pp.remove(order);
        }
    }

    public void display(ActionEvent actionEvent) {
        if(order == null){
            alert("Bạn cần tạo đơn hàng trước");
        }else {
            popup();
            pp.show(order);
        }
    }

    public void exit(ActionEvent actionEvent) {
        System.exit(0);
    }
}
