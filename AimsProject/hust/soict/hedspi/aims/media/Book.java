/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hust.soict.hedspi.aims.media;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

/**
 * @author ADMIN
 */
public class Book extends Media implements Comparable<Book> {
    //    private String title;
//    private String category;
//    private float cost;
    private List<String> authors = new ArrayList<>();
    private String content;
    private TreeMap<String, Integer> wordFrequency;
    private List<String> contentTokens;

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public Book() {

    }

    public void addAuthor(String authorName) {
        if (!(authors.contains(authorName))) {
            authors.add(authorName);
        }
    }

    public void removeAuthor(String authorName) {
        if ((authors.contains(authorName))) {
            authors.remove(authorName);
        }
    }

    Book(String title) {
        super(title);
    }

    Book(String title, String category) {
        super(title, category);
    }

    public Book(String title, String category, List<String> authors) {
        super(title, category);
        this.authors = authors;
        //Todo: check author condition
    }

    @Override
    public int compareTo(Book o) {
        return this.id - o.getId();
    }

    public void processContent() {
        contentTokens = new ArrayList<>(List.of(this.content.split(" ,./")));
        Collections.sort(contentTokens);
        wordFrequency = new TreeMap<>();
        contentTokens.forEach(t -> {
            if (wordFrequency.containsKey(t))
                wordFrequency.replace(t, wordFrequency.get(t) + 1);
            else
                wordFrequency.put(t, 1);
        });
    }

    @Override
    public String toString() {
        return "Book{" + "title: " + title + ", category: " + category + ", author: " + authors.get(0) + '}';
    }
}
